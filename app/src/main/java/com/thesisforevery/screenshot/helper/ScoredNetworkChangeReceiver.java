package com.thesisforevery.screenshot.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;
import android.util.Log;

import static android.content.Context.WIFI_SERVICE;

public class ScoredNetworkChangeReceiver extends BroadcastReceiver {

    public interface OnConnectionChangeListener {
        void onConnected();
        void onDisConnected();
    }

    OnConnectionChangeListener onConnectionChangeListener;

    public ScoredNetworkChangeReceiver(OnConnectionChangeListener mOnConnectionChangeListener) {
        this.onConnectionChangeListener = mOnConnectionChangeListener;
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        WifiManager wm = (WifiManager) context.getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        Log.d("NetworkChangeReceiver", "IP ADDRESS: "+ ip);

        int status = NetworkUtil.getConnectivityStatusString(context);
        if (status == NetworkUtil.NETWORK_STATUS_NOT_CONNECTED) {
            Log.e("NetworkChangeReceiver", "DISCONNECTED");
            onConnectionChangeListener.onDisConnected();
        } else {

            if (ip.equals(Constants.WIFI_MODULE_IP)) {
                Log.e("NetworkChangeReceiver", "CONNECTED");
                onConnectionChangeListener.onConnected();
            } else {
                Log.e("NetworkChangeReceiver", "DISCONNECTED");
                onConnectionChangeListener.onDisConnected();
            }

        }
    }
}