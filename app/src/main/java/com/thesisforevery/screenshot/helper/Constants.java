package com.thesisforevery.screenshot.helper;

public class Constants  {
    // TODO: Change Original IP when testing with scoreboard (192.168.4.1)
    public static String WIFI_MODULE_IP = "192.168.232.2";
    public static int WIFI_MODULE_PORT = 21567;

    public static final String WIN = "Win";
    public static final String LOSE = "Lose";

    public static final String CONNECTIVITY_INTENT_FILTER = "android.net.conn.CONNECTIVITY_CHANGE";

    // Game Types
    public static final int BADMINTON = 1;
    public static final int BASKETBALL = 2;
    public static final int TABLE_TENNIS = 3;
    public static final int VOLLEYBALL = 4;
}
