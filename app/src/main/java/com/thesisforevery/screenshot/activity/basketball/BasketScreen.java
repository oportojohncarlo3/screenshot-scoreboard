package com.thesisforevery.screenshot.activity.basketball;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.thesisforevery.screenshot.R;
import com.thesisforevery.screenshot.activity.MainActivity;
import com.thesisforevery.screenshot.helper.Constants;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class BasketScreen extends AppCompatActivity {
    Socket socket = null;
    String NameOne, NameTwo;
    public static String CMD = "";
    public static final String BAS_DATAONE = "BASDATAONE";
    public static final String BAS_DATATWO = "BASDATATWO";
    private EditText TeamOne, TeamTwo;
    private Button Basketaccept, home_btn;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_basket_screen);
        // initializing the views
        ViewDeclaration();
        Basketaccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TeamOneData();
                mHandler.postDelayed(TeamTwoData, 1000);
            }
        });

        home_btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            CMD = "MainMenu";
                                            DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
                                            dataSendtoRasp.execute();
                                            startActivity(new Intent(BasketScreen.this, MainActivity.class));
                                        }
                                    }
        );
    }

    public void ViewDeclaration() {
        home_btn = findViewById(R.id.btn_home);
        Basketaccept = findViewById(R.id.VacceptBtn);
        TeamOne = findViewById(R.id.tV_TeamTwo_Foul);
        TeamTwo = findViewById(R.id.TeamTwo);
        TeamOne.addTextChangedListener(loginTextWatcher);
        TeamTwo.addTextChangedListener(loginTextWatcher);
    }

    public void TeamOneData() {
        NameOne = TeamOne.getText().toString();
        CMD = "bb_p1_name" + NameOne;
        DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
        dataSendtoRasp.execute();
    }

    private Runnable TeamTwoData = new Runnable() {
        @Override
        public void run() {
            NameTwo = TeamTwo.getText().toString();
            CMD = "bb_p2_name" + NameTwo;
            DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
            dataSendtoRasp.execute();
            openBasketScoreBoard();
        }
    };
    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String NameOne = TeamOne.getText().toString();
            String NameTwo = TeamTwo.getText().toString();
            ///CHECK IF THE EDITTEXT HAS DATA ON IT
            Basketaccept.setEnabled(!NameOne.isEmpty() && !NameTwo.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public void openBasketScoreBoard() {
        NameOne = TeamOne.getText().toString();
        NameTwo = TeamTwo.getText().toString();
        Intent data = new Intent(this, BasketBall_Board.class);
        data.putExtra(BAS_DATAONE, NameOne);
        data.putExtra(BAS_DATATWO, NameTwo);
        startActivity(data);
    }

    public class DataSendtoRasp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                InetAddress inetAddress = InetAddress.getByName(Constants.WIFI_MODULE_IP);
                socket = new java.net.Socket(inetAddress, Constants.WIFI_MODULE_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(CMD);
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
