package com.thesisforevery.screenshot.activity.volleyball;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.thesisforevery.screenshot.R;
import com.thesisforevery.screenshot.activity.MainActivity;
import com.thesisforevery.screenshot.helper.Constants;
import com.thesisforevery.screenshot.helper.ScoredNetworkChangeReceiver;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class VolleyBall_Board extends AppCompatActivity implements View.OnClickListener {
    Socket socket = null;
    CountDownTimer TimeOut;
    AnimationDrawable anim;
    public static String CMD = "";
    private int counterOne, counterTwo;
    private int SET_TO_WIN = 3;
    private int teamOneSetWonCounter, teamTwoSetWonCounter;
    private int gameSetCounter = 1;
    int redTimeOutCount = 2;
    int blueTimeOutCount = 2;
    private ImageView BlueServe, RedServe;
    private Button previous, btn_home;
    private Button BtnPOne, BtnMOne, BtnPTwo, BtnMTwo, Btn_TimeOut_blue, Btn_TimeOut_red, BtnClrAll;
    private TextView TeamOne, TeamTwo, WINBLue, WINRed;
    private TextView ScoreBlue, ScoreRed, SetWonRed, SetWonBlue, SetDisplay;
    private TextView setOnePerSet, setTwoPerSet, setThreePerSet, setFourPerSet, setFivePerSet;
    private TextView setOnePerSet2, setTwoPerSet2, setThreePerSet2, setFourPerSet2, setFivePerSet2;
    private TextView Text_TimeoutBLue, Text_TimeoutRed, TextChangeTimeout;
    private AlertDialog dialog;
    private ScoredNetworkChangeReceiver scoredNetworkChangeReceiver;
    boolean enableButtons;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_volley_ball__board);
        ViewDeclaration();
        BtnDeclaration();
        AnimationDisplay();

        scoredNetworkChangeReceiver = new ScoredNetworkChangeReceiver(new ScoredNetworkChangeReceiver.OnConnectionChangeListener() {
            @Override
            public void onConnected() {
                Toast.makeText(VolleyBall_Board.this, "CONNECTED", Toast.LENGTH_SHORT).show();
                enableButtons = true;
                isToEnableButtons();
            }
            @Override
            public void onDisConnected() {
                Toast.makeText(VolleyBall_Board.this, "DISCONNECTED", Toast.LENGTH_SHORT).show();
                enableButtons = false;
                isToEnableButtons();
            }
        });
        // Create an IntentFilter instance.
        IntentFilter intentFilter = new IntentFilter();
        // Add network connectivity change action.
        intentFilter.addAction(Constants.CONNECTIVITY_INTENT_FILTER);
        // Set broadcast receiver priority.
        intentFilter.setPriority(100);
        registerReceiver(scoredNetworkChangeReceiver, intentFilter);
    }
    public void isToEnableButtons() {
        BlueServe.setEnabled(enableButtons);
        RedServe.setEnabled(enableButtons);
        BtnPOne.setEnabled(enableButtons);
        BtnMOne.setEnabled(enableButtons);
        BtnPTwo.setEnabled(enableButtons);
        BtnMTwo.setEnabled(enableButtons);
        previous.setEnabled(enableButtons);
        btn_home.setEnabled(enableButtons);
        BtnClrAll.setEnabled(enableButtons);
        Btn_TimeOut_blue.setEnabled(enableButtons);
        Btn_TimeOut_red.setEnabled(enableButtons);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // If the broadcast receiver is not null then unregister it.
        // This action is better placed in activity onDestroy() method.
        if (this.scoredNetworkChangeReceiver != null) {
            unregisterReceiver(this.scoredNetworkChangeReceiver);
        }

    }
    public void ViewDeclaration() {
        ////////////TEXTVIEW////////////////////
        ///////////DISPLAYS TEAM NAME//////////
        Intent intent = getIntent();
        String NameOne = intent.getStringExtra(VolleyScreen.DATAONE);
        String NameTwo = intent.getStringExtra(VolleyScreen.DATATWO);
        TeamOne = findViewById(R.id.tV_TeamTwo_Foul);
        TeamTwo = findViewById(R.id.TeamTwo);
        TeamOne.setText(NameOne);
        TeamTwo.setText(NameTwo);
/////////////TEXTVIEW AND EDIT TEXT FOR TIME OUT ///////////
        TextChangeTimeout = findViewById(R.id.TextChangeTimeout);
        Text_TimeoutBLue = findViewById(R.id.Text_TimeoutBLue);
        Text_TimeoutRed = findViewById(R.id.Text_TimeoutRed);
////////////TEXTVIEW FOR SCORE AND SET/////////////
        ScoreBlue = findViewById(R.id.tV_TimeOut_Blue);
        ScoreRed = findViewById(R.id.tV_TimeOut_Red);
        SetWonRed = findViewById(R.id.SetWonRed);
        SetWonBlue = findViewById(R.id.SetWonBlue);
        SetDisplay = findViewById(R.id.SetDisplay);
        WINBLue = findViewById(R.id.text2);
        WINRed = findViewById(R.id.text);
/////////////SCORE PER SET OF BLUE///////////////
        setOnePerSet = findViewById(R.id.setOnePerSet);
        setTwoPerSet = findViewById(R.id.setTwoPerSet);
        setThreePerSet = findViewById(R.id.setThreePerSet);
        setFourPerSet = findViewById(R.id.setFourPerSet);
        setFivePerSet = findViewById(R.id.setFivePerSet);
/////////////SCORE PER SET OF RED//////////////////
        setOnePerSet2 = findViewById(R.id.setOnePerSet2);
        setTwoPerSet2 = findViewById(R.id.setTwoPerSet2);
        setThreePerSet2 = findViewById(R.id.setThreePerSet2);
        setFourPerSet2 = findViewById(R.id.setFourPerSet2);
        setFivePerSet2 = findViewById(R.id.setFivePerSet2);

        TimeOut = new CountDownTimer(30000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                TextChangeTimeout.setText(millisUntilFinished / 1000 + "");
                Btn_TimeOut_blue.setEnabled(enableButtons);
                Btn_TimeOut_red.setEnabled(enableButtons);
                BtnPOne.setEnabled(enableButtons);
                BtnMOne.setEnabled(enableButtons);
                BtnPTwo.setEnabled(enableButtons);
                BtnMTwo.setEnabled(enableButtons);
            }
            @Override
            public void onFinish() {
                TextChangeTimeout.setText("TIME OUT LEFT");
                Btn_TimeOut_blue.setEnabled(true);
                Btn_TimeOut_red.setEnabled(true);
                BtnPOne.setEnabled(true);
                BtnMOne.setEnabled(true);
                BtnPTwo.setEnabled(true);
                BtnMTwo.setEnabled(true);
            }
        };
    }
    public void BtnDeclaration() {
        BlueServe = findViewById(R.id.BlueServe);
        RedServe = findViewById(R.id.RedServe);
//////////BUTTON FOR TEAM BLUE INCREMENT AND DECREMENT///
        BtnPOne = findViewById(R.id.BtnPlusOne);
        BtnMOne = findViewById(R.id.BtnMinOne);
/////////BUTTON FOR TEAM RED INCREMENT AND DECREMENT////
        BtnPTwo = findViewById(R.id.BtnPlusTwo);
        BtnMTwo = findViewById(R.id.BtnMinTwo);
///////BUTTON TO CLEAR DISPLAY ,GO BACK TO MAIN MENU AND PREVIOUS/////////
        previous = findViewById(R.id.previous);
        btn_home = findViewById(R.id.btn_home);
        BtnClrAll = findViewById(R.id.BtnClear);
/////////BUTTON FOR TIMEOUT OF BOTH TEAM//////////
        Btn_TimeOut_blue = findViewById(R.id.Btn_TimeOut_blue);
        Btn_TimeOut_red = findViewById(R.id.Btn_TimeOut_red);
///////////BUTTON SET TO LISTENER////////////////
        BlueServe.setOnClickListener(this);
        RedServe.setOnClickListener(this);
        BtnPOne.setOnClickListener(this);
        BtnMOne.setOnClickListener(this);
        BtnPTwo.setOnClickListener(this);
        BtnMTwo.setOnClickListener(this);
        previous.setOnClickListener(this);
        btn_home.setOnClickListener(this);
        BtnClrAll.setOnClickListener(this);
        Btn_TimeOut_blue.setOnClickListener(this);
        Btn_TimeOut_red.setOnClickListener(this);
    }
    public void  AnimationDisplay()
    {
        BlueServe = findViewById(R.id.BlueServe);
        if (BlueServe == null) throw new AssertionError();
        BlueServe.setBackgroundResource(R.drawable.animation);
        anim = (AnimationDrawable) BlueServe.getBackground();
        anim.start();
        RedServe = findViewById(R.id.RedServe);
        if (RedServe == null) throw new AssertionError();
        RedServe.setBackgroundResource(R.drawable.animation);
        anim = (AnimationDrawable) RedServe.getBackground();
        anim.start();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            ////////////BUTTON FOR TIME OUT OF BOTH TEAM////////////////////////////
            case R.id.Btn_TimeOut_blue:
                TimeOut.start();
                if (blueTimeOutCount != 0) {
                    blueTimeOutCount--;
                    if (blueTimeOutCount == 0) {
                        Btn_TimeOut_blue.setEnabled(false);
                    }
                }
                Text_TimeoutBLue.setText(blueTimeOutCount + "");
                CMD = "vb_p1_timeoutMinOne";
                DataSendtoRasp vb_p1_timeoutMinOne = new DataSendtoRasp();
                vb_p1_timeoutMinOne.execute();
                break;
            case R.id.Btn_TimeOut_red:
                TimeOut.start();
                if (redTimeOutCount != 0) {
                    redTimeOutCount--;
                    if (redTimeOutCount == 0) {
                        Btn_TimeOut_red.setEnabled(false);
                    }
                }
                Text_TimeoutRed.setText(redTimeOutCount + "");
                CMD = "vb_p2_timeoutMinOne";
                DataSendtoRasp vb_p2_timeoutMinOne = new DataSendtoRasp();
                vb_p2_timeoutMinOne.execute();
                break;

            case R.id.BlueServe:
                RedServe.setVisibility(View.INVISIBLE);
                BlueServe.setEnabled(false);
                RedServe.setEnabled(false);
                BtnPOne.setEnabled(true);
                BtnMOne.setEnabled(true);
                BtnPTwo.setEnabled(true);
                BtnMTwo.setEnabled(true);
                Btn_TimeOut_blue.setEnabled(true);
                Btn_TimeOut_red.setEnabled(true);
                break;
            case R.id.RedServe:
                BlueServe.setVisibility(View.INVISIBLE);
                BlueServe.setEnabled(false);
                RedServe.setEnabled(false);
                BtnPOne.setEnabled(true);
                BtnMOne.setEnabled(true);
                BtnPTwo.setEnabled(true);
                BtnMTwo.setEnabled(true);
                Btn_TimeOut_blue.setEnabled(true);
                Btn_TimeOut_red.setEnabled(true);
                break;
////////////////BUTTON FOR INCREMENT AND DECREMENT OF TEAM BLUE/////////////////
            case R.id.BtnPlusOne:
                CMD = "vb_p1_scorePlusOne";
                DataSendtoRasp vb_p1_scorePlusOne = new DataSendtoRasp();
                vb_p1_scorePlusOne.execute();
                IncrementTeamBlue();
                break;

            case R.id.BtnMinOne:
                CMD = "vb_p1_scoreMinusOne";
                DataSendtoRasp vb_p1_scoreMinusOne = new DataSendtoRasp();
                vb_p1_scoreMinusOne.execute();
                counterOne--;
                if (counterOne < 1) {
                    counterOne = 0;
                }
                ScoreBlue.setText(counterOne + "");
                break;
            //////////////////BUTTON FOR INCREMENT AND DECREMENT OF TEAM RED//////////////////
            case R.id.BtnPlusTwo:
                CMD = "vb_p2_scorePlusOne";
                DataSendtoRasp vb_p2_scorePlusOne = new DataSendtoRasp();
                vb_p2_scorePlusOne.execute();
                IncrementTeamRed();
                break;

            case R.id.BtnMinTwo:
                CMD = "vb_p2_scoreMinusOne";
                DataSendtoRasp vb_p2_scoreMinusOne = new DataSendtoRasp();
                vb_p2_scoreMinusOne.execute();
                counterTwo--;
                if (counterTwo < 1) {
                    counterTwo = 0;
                }
                ScoreRed.setText(counterTwo + "");
                break;
/////////////////BUTTON TO CLEAR ALL DISPLAYED    ///////////////
            case R.id.BtnClear:
                gameSetCounter = 1;
                counterOne = 0;
                counterTwo = 0;
                teamOneSetWonCounter = 0;
                teamTwoSetWonCounter = 0;
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
/////////////CLEAR SCORE FOR BOTH TEAM///////////////////////////////
                ScoreBlue.setText(counterOne + "");
                ScoreRed.setText(counterTwo + "");
////////////////////CLEAR SCORE PER SET BOTH TEAM///////////////////////////////
                setOnePerSet.setText("SET 1: " + counterOne + "");
                setTwoPerSet.setText("SET 2: " + counterOne + "");
                setThreePerSet.setText("SET 3: " + counterOne + "");
                setFourPerSet.setText("SET 4: " + counterOne + "");
                setFivePerSet.setText("SET 5: " + counterOne + "");
                setOnePerSet2.setText("SET 1: " + counterTwo + "");
                setTwoPerSet2.setText("SET 2: " + counterTwo + "");
                setThreePerSet2.setText("SET 3: " + counterTwo + "");
                setFourPerSet2.setText("SET 4: " + counterTwo + "");
                setFivePerSet2.setText("SET 5: " + counterTwo + "");
///////////////SERVER BUTTON//////////////////////////////
                BlueServe.setEnabled(true);
                RedServe.setEnabled(true);
                RedServe.setVisibility(View.VISIBLE);
                BlueServe.setVisibility(View.VISIBLE);

              /*
                BUTTON IS SET TO FALSE TO BE DISABLED AND WILL BE ENABLED IF THE USER
                INPUT TIMEOUT LIMIT FOR BOTH TEAM
                                   */
                BtnClrAll.setEnabled(false);
                BtnPOne.setEnabled(false);
                BtnMOne.setEnabled(false);
                BtnPTwo.setEnabled(false);
                BtnMTwo.setEnabled(false);
                Btn_TimeOut_blue.setEnabled(false);
                Btn_TimeOut_red.setEnabled(false);
                break;


            case R.id.previous:
                dialog = new AlertDialog.Builder(this).create();
                dialog.setTitle("RENAME TEAM?");
                dialog.setMessage("DATA WILL BE LOST");
                dialog.setButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Previous();
                    }
                });
                dialog.setButton2("NO,", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.btn_home:
                dialog = new AlertDialog.Builder(this).create();
                dialog.setTitle("Back to MAIN SCREEN");
                dialog.setMessage("You sure?");
                dialog.setButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainMenu();
                    }
                });
                dialog.setButton2("NO,", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

                break;
        }
    }

    public class DataSendtoRasp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                InetAddress inetAddress = InetAddress.getByName(Constants.WIFI_MODULE_IP);
                socket = new java.net.Socket(inetAddress, Constants.WIFI_MODULE_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(CMD);
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    public void Previous() {
        startActivity(new Intent(VolleyBall_Board.this, VolleyScreen.class));
    }
    @Override
    public void onBackPressed() {
        //disable back button
    }
    public void MainMenu() {
        CMD = "MainMenu";
        DataSendtoRasp MainMenu = new DataSendtoRasp();
        MainMenu.execute();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void IncrementTeamBlue() {
        counterOne++;
        if (counterOne >= 25) {
            counterOne = 25;
            if (teamTwoSetWonCounter != 3) {
                if (teamOneSetWonCounter < 3) {
                    teamOneSetWonCounter++;
                }
                if (teamOneSetWonCounter >= 3) {
                    // Team Two Wins
                    Intent intent = getIntent();
                    String NameOne = intent.getStringExtra(VolleyScreen.DATAONE);
                    Toast.makeText(this, "Team " + NameOne + " Wins", Toast.LENGTH_LONG).show();
                    ScoreBlue.setText("" + SET_TO_WIN);
                    WINBLue.setText(Constants.WIN);
                    WINRed.setText(Constants.LOSE);
                    BtnPOne.setEnabled(false);
                    BtnMOne.setEnabled(false);
                    BtnPTwo.setEnabled(false);
                    BtnMTwo.setEnabled(false);
                    Btn_TimeOut_blue.setEnabled(false);
                    Btn_TimeOut_red.setEnabled(false);
                    BtnClrAll.setEnabled(true);
                } else {
                    gameSetCounter++;
                    SetDisplay.setText(gameSetCounter + "");
                    SetWonBlue.setText(teamOneSetWonCounter + "");
                }
            } else {
                // Team RED already won
            }
            if (gameSetCounter == 2) {
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                setOnePerSet.setText("SET 1:" + counterOne + "");
                setOnePerSet2.setText("SET 1:" + counterTwo + "");
            }
            if (gameSetCounter == 3) {
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                setTwoPerSet.setText("SET 2:" + counterOne + "");
                setTwoPerSet2.setText("SET 2:" + counterTwo + "");
            }
            if (gameSetCounter == 4) {
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                setThreePerSet.setText("SET 3:" + counterOne + "");
                setThreePerSet2.setText("SET 3:" + counterTwo + "");
            }
            if (gameSetCounter == 5) {
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                setFourPerSet.setText("SET 4:" + counterOne + "");
                setFourPerSet2.setText("SET 4:" + counterTwo + "");
            }
            if (gameSetCounter == 6) {
                gameSetCounter = 5;
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                SetDisplay.setText(gameSetCounter + "");
                setFivePerSet.setText("SET 5: " + counterOne + "");
                setFivePerSet2.setText("SET 5: " + counterTwo + "");
            }
            counterOne = 0;
            counterTwo = 0;
        }
        ScoreBlue.setText(counterOne + "");
        ScoreRed.setText(counterTwo + "");
        RedServe.setVisibility(View.INVISIBLE);
        BlueServe.setVisibility(View.VISIBLE);
    }
    public void IncrementTeamRed() {
        counterTwo++;
        if (counterTwo >= 25) {
            counterTwo = 25;
            if (teamOneSetWonCounter != 3) {
                if (teamTwoSetWonCounter < 3) {
                    teamTwoSetWonCounter++;
                }
                if (teamTwoSetWonCounter >= 3) {
                    // Team Two Wins
                    Intent intent = getIntent();
                    String NameTwo = intent.getStringExtra(VolleyScreen.DATATWO);
                    Toast.makeText(this, "Team " + NameTwo + " Wins", Toast.LENGTH_LONG).show();
                    SetWonRed.setText("" + SET_TO_WIN);
                    WINBLue.setText(Constants.LOSE);
                    WINRed.setText(Constants.WIN);
                    BtnPOne.setEnabled(false);
                    BtnMOne.setEnabled(false);
                    BtnPTwo.setEnabled(false);
                    BtnMTwo.setEnabled(false);
                    Btn_TimeOut_blue.setEnabled(false);
                    Btn_TimeOut_red.setEnabled(false);
                    BtnClrAll.setEnabled(true);
                } else {
                    gameSetCounter++;
                    SetDisplay.setText(gameSetCounter + "");
                    SetWonRed.setText(teamTwoSetWonCounter + "");
                }
            } else {
                // Team One already won
            }
            if (gameSetCounter == 2) {
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                setOnePerSet.setText("SET 1:" + counterOne + "");
                setOnePerSet2.setText("SET 1:" + counterTwo + "");
            }
            if (gameSetCounter == 3) {
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                setTwoPerSet.setText("SET 2:" + counterOne + "");
                setTwoPerSet2.setText("SET 2:" + counterTwo + "");
            }
            if (gameSetCounter == 4) {
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                setThreePerSet.setText("SET 3:" + counterOne + "");
                setThreePerSet2.setText("SET 3:" + counterTwo + "");
            }
            if (gameSetCounter == 5) {
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                setFourPerSet.setText("SET 4:" + counterOne + "");
                setFourPerSet2.setText("SET 4:" + counterTwo + "");
            }
            if (gameSetCounter == 6) {
                gameSetCounter = 5;
                redTimeOutCount = 2;
                blueTimeOutCount = 2;
                Text_TimeoutBLue.setText("2");
                Text_TimeoutRed.setText("2");
                SetDisplay.setText(gameSetCounter + "");
                setFivePerSet.setText("SET 5: " + counterOne + "");
                setFivePerSet2.setText("SET 5: " + counterTwo + "");
            }
            counterOne = 0;
            counterTwo = 0;
        }
        ScoreBlue.setText(counterOne + "");
        ScoreRed.setText(counterTwo + "");
        RedServe.setVisibility(View.VISIBLE);
        BlueServe.setVisibility(View.INVISIBLE);
    }
}
