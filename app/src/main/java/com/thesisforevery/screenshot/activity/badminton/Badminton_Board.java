package com.thesisforevery.screenshot.activity.badminton;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.thesisforevery.screenshot.R;
import com.thesisforevery.screenshot.activity.MainActivity;
import com.thesisforevery.screenshot.helper.Constants;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class Badminton_Board extends AppCompatActivity implements View.OnClickListener {
    private AppCompatActivity activity = Badminton_Board.this;
    Socket socket = null;
    public static String CMD = "";
    private int counterOne,counterTwo;
    private int SetcountOne,SetcountTwo;
    private int SetcountALL = 1;
    private int  SET_TO_WIN = 2;
    private AlertDialog dialog;
    private TextView PlayerOne, PlayerTwo, PlayerThree, PlayerFour;
    private TextView ScoreDisOne, ScoreDisTwo, SetWonOne, SetWonTwo, SetDisplay,WINBLue,WINRed;
    private TextView setOnePerSet,setTwoPerSet,setThreePerSet,setOnePerSet2,setTwoPerSet2,setThreePerSet2;
    private Button BtnPlusOne,BtnMinOne,BtnPlusTwo,BtnMinTwo,BtnClear;
    private Button Btnhome,previous,Btnsave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_badminton_board);
        ViewDeclaration();
        BtnUsedDeclaration();
    }
   public void ViewDeclaration(){
       Intent intent = getIntent();
       String NameOne = intent.getStringExtra(BadminScreen.DATAONE);
       String NameTwo = intent.getStringExtra(BadminScreen.DATATWO);
       String NameThree = intent.getStringExtra(BadminScreen.DATATHREE);
       String NameFour = intent.getStringExtra(BadminScreen.DATAFOUR);
       PlayerOne = findViewById(R.id.tV_TeamTwo_Foul);
       PlayerTwo = findViewById(R.id.TeamTwo);
       PlayerThree = findViewById(R.id.PlayerThree);
       PlayerFour = findViewById(R.id.PlayerFour);
       PlayerOne.setText(NameOne);
       PlayerTwo.setText(NameTwo);
       PlayerThree.setText(NameThree);
       PlayerFour.setText(NameFour);
       ///////////TEXTVIEW DECLARATION///////////////////////
       ///////////////SCORE DISPLAY/////////
       ScoreDisOne = findViewById(R.id.tV_TimeOut_Blue);
       ScoreDisTwo = findViewById(R.id.tV_TimeOut_Red);
       WINBLue = findViewById(R.id.text2);
       WINRed = findViewById(R.id.text);
       SetWonOne = findViewById(R.id.SetWonOne);
       SetWonTwo = findViewById(R.id.SetWonTwo);
       SetDisplay = findViewById(R.id.SetDisplay);

///////////////////SCORE PER SET OF BLUE /////////////////////////
       setOnePerSet = findViewById(R.id.setOnePerSet);
       setTwoPerSet = findViewById(R.id.setTwoPerSet);
       setThreePerSet = findViewById(R.id.setThreePerSet);
/////////////////SET PERSET OF RED  /////////////////////////
       setOnePerSet2 = findViewById(R.id.setOnePerSet2);
       setTwoPerSet2 = findViewById(R.id.setTwoPerSet2);
       setThreePerSet2 = findViewById(R.id.setThreePerSet2);
   }
 public void   BtnUsedDeclaration(){
//////BUTTON TOOLS///////
     Btnhome = findViewById(R.id.btn_home);
     previous = findViewById(R.id.previous);
     Btnsave = findViewById(R.id.btn_save);


//////////////////////BUTTON///////////
     BtnPlusOne = findViewById(R.id.BtnPlusOne);
     BtnMinOne = findViewById(R.id.BtnMinOne);
     BtnPlusTwo = findViewById(R.id.BtnPlusTwo);
     BtnMinTwo = findViewById(R.id.BtnMinTwo);
     BtnClear = findViewById(R.id.BtnClear);
     ///////////BUTTON SET TO BE LISTENER//////////////
     BtnPlusOne.setOnClickListener(this);
     BtnMinOne.setOnClickListener(this);
     BtnPlusTwo.setOnClickListener(this);
     BtnMinTwo.setOnClickListener(this);
     BtnClear.setOnClickListener(this);
     Btnhome.setOnClickListener(this);
     previous.setOnClickListener(this);
     Btnsave.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

 //////////////////BUTTONS FOR TEAM BLUE //////////////////////////////////
            case R.id.BtnPlusOne:
                counterOne++;
                CMD = "bd_p1_scorePlusOne";
                DataSendtoRasp bd_p1_scorePlusOne = new DataSendtoRasp();
                bd_p1_scorePlusOne.execute();
                if (counterOne >=30) {
                    counterOne = 30;
                    if(SetcountTwo !=2)
                    {
                        if (SetcountOne <2){
                            SetcountOne++;
                        }
                        if (SetcountOne ==2)
                        {
                            // TEAM ONE WINS
                            Intent intent = getIntent();
                            String NameOne = intent.getStringExtra(BadminScreen.DATAONE);
                            String NameThree = intent.getStringExtra(BadminScreen.DATATHREE);
                            Toast.makeText(this, "Team " +NameOne+" "+NameThree+" Wins", Toast.LENGTH_LONG).show();
                            SetWonOne.setText("" + SET_TO_WIN);
                            WINBLue.setText(Constants.WIN);
                        WINRed.setText(Constants.LOSE);
                            BtnPlusOne.setEnabled(false);
                            BtnMinOne.setEnabled(false);
                            BtnPlusTwo.setEnabled(false);
                            BtnMinTwo.setEnabled(false);
                        } else {
                            SetcountALL++;
                            SetDisplay.setText(SetcountALL + "");
                            SetWonOne.setText(SetcountOne + "");
                        }
                        if(SetcountALL == 2)
                        {
                            setOnePerSet.setText("SET 1:"+ counterOne + "");
                            setOnePerSet2.setText("SET 1:"+ counterTwo + "");
                        }
                        if(SetcountALL == 3){

                            setTwoPerSet.setText("SET 2:"+counterOne + "");
                            setTwoPerSet2.setText("SET 2:"+counterTwo + "");
                        }
                        if(SetcountALL > 3){
                            SetcountALL = 3;
                            SetDisplay.setText(SetcountALL + "");
                            setThreePerSet.setText("SET 3:"+counterOne + "");
                            setThreePerSet2.setText("SET 3:"+counterTwo + "");
                        }
                        counterOne = 0;
                        counterTwo = 0;
                    }
                    else {
                    }
                }
                ScoreDisOne.setText(counterOne + "");
                ScoreDisTwo.setText(counterTwo + "");
                break;

            case R.id.BtnMinOne:
                counterOne--;
                CMD = "bd_p1_scoreMinusOne";
                DataSendtoRasp bd_p1_scoreMinusOne = new DataSendtoRasp();
                bd_p1_scoreMinusOne.execute();
                if (counterOne < 1) {
                    counterOne = 0;
                }
                ScoreDisOne.setText(counterOne + "");
                break;
/////////////////////BUTTONS FOR TEAM RED///////////////////////
            case R.id.BtnPlusTwo:
                counterTwo++;
                CMD = "bd_p2_scorePlusOne";
                DataSendtoRasp bd_p2_scorePlusOne = new DataSendtoRasp();
                bd_p2_scorePlusOne.execute();
                if (counterTwo >= 30) {
                    counterTwo = 30;
                    if(SetcountOne !=2)
                    {
                        if (SetcountTwo <2){
                            SetcountTwo++;
                        }
                        if (SetcountTwo ==2)
                        {
                            // TEAM TWO WINS
                            Intent intent = getIntent();
                            String NameTwo = intent.getStringExtra(BadminScreen.DATATWO);
                            String NameFour = intent.getStringExtra(BadminScreen.DATAFOUR);
                            Toast.makeText(this, "Team " +NameTwo+" "+NameFour+" Wins", Toast.LENGTH_LONG).show();
                            SetWonTwo.setText("" + SET_TO_WIN);
                            WINBLue.setText(Constants.LOSE);
                            WINRed.setText(Constants.WIN);
                            BtnPlusOne.setEnabled(false);
                            BtnMinOne.setEnabled(false);
                            BtnPlusTwo.setEnabled(false);
                            BtnMinTwo.setEnabled(false);
                        } else {
                            SetcountALL++;
                            SetDisplay.setText(SetcountALL + "");
                            SetWonTwo.setText(SetcountTwo + "");
                        }
                        if(SetcountALL == 2)
                        {
                            setOnePerSet.setText("SET 1:"+ counterOne + "");
                            setOnePerSet2.setText("SET 1:"+ counterTwo + "");
                        }
                        if(SetcountALL == 3)
                        {
                            setTwoPerSet.setText("SET 2:"+counterOne + "");
                            setTwoPerSet2.setText("SET 2:"+counterTwo + "");
                        }
                        if(SetcountALL > 3)
                        {
                            SetcountALL = 3;
                            SetDisplay.setText(SetcountALL + "");
                            setThreePerSet.setText("SET 3:"+counterOne + "");
                            setThreePerSet2.setText("SET 3:"+counterTwo + "");
                        }
                        counterOne = 0;
                        counterTwo = 0;
                    }
                    else {
                    }
                }
                ScoreDisOne.setText(counterOne + "");
                ScoreDisTwo.setText(counterTwo + "");
                break;
            case R.id.BtnMinTwo:
                counterTwo--;
                CMD = "bd_p2_scoreMinusOne";
                DataSendtoRasp bd_p2_scoreMinusOne = new DataSendtoRasp();
                bd_p2_scoreMinusOne.execute();
                if (counterTwo < 1) {
                    counterTwo = 0;
                }
                ScoreDisTwo.setText(counterTwo + "");
                break;
//////////////////////////////CLEAR ALL DISPLAYED//////////////////////////////
            case R.id.BtnClear:
                CMD = "bad_m_Clear";
                DataSendtoRasp bad_m_Clear = new DataSendtoRasp();
                bad_m_Clear.execute();
                counterOne = 0;
                counterTwo = 0;
                SetcountTwo = 0;
                SetcountOne = 0;
                SetcountALL = 1;
                SetWonOne.setText("0");
                SetWonTwo.setText("0");
                ScoreDisOne.setText(counterOne + "");
                setOnePerSet.setText("SET 1: " +counterOne + "");
                setTwoPerSet.setText("SET 2: " +counterOne + "");
                setThreePerSet.setText("SET 3: " +counterOne + "");
                ScoreDisTwo.setText(counterTwo + "");
                setOnePerSet2.setText("SET 1: " +counterOne + "");
                setTwoPerSet2.setText("SET 2: " +counterOne + "");
                setThreePerSet2.setText("SET 3: " +counterOne + "");
                BtnPlusOne.setEnabled(true);
                BtnMinOne.setEnabled(true);
                BtnPlusTwo.setEnabled(true);
                BtnMinTwo.setEnabled(true);
                break;
            case R.id.btn_home:
                dialog = new AlertDialog.Builder(this).create();
                dialog.setTitle("Back to MAIN SCREEN");
                dialog.setMessage("You sure?");
                dialog.setButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainMenu();
                    }
                });
                dialog.setButton2("NO,", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.previous:
                dialog = new AlertDialog.Builder(this).create();
                dialog.setTitle("RENAME TEAM?");
                dialog.setMessage("DATA WILL BE LOST");
                dialog.setButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Previous();
                    }
                });
                dialog.setButton2("NO,", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
        }
    }
    @Override
    public void onBackPressed(){

    }
    public void Previous() {
        startActivity(new Intent(Badminton_Board.this, BadminScreen.class));
    }
    public class DataSendtoRasp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                InetAddress inetAddress = InetAddress.getByName(Constants.WIFI_MODULE_IP);
                socket = new java.net.Socket(inetAddress, Constants.WIFI_MODULE_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(CMD);
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    public void MainMenu() {
        CMD = "MainMenu";
        DataSendtoRasp MainMenu = new DataSendtoRasp();
        MainMenu.execute();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}