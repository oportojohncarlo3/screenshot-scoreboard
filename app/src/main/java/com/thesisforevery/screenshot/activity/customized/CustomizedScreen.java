package com.thesisforevery.screenshot.activity.customized;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.thesisforevery.screenshot.R;
import com.thesisforevery.screenshot.activity.MainActivity;
import com.thesisforevery.screenshot.helper.Constants;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class CustomizedScreen extends AppCompatActivity {
    public static final String DATAPLAYERONE = "DATAPLAYERONE";
    public static final String DATAPLAYERTWO = "DATAPLAYERTWO";
    public static final String DATAPLAYERTHREE = "DATAPLAYERTHREE";
    public static final String DATAPLAYERFOUR = "DATAPLAYERFOUR";
    Socket socket = null;
    // public static String wifiModuleIp = "192.168.4.1";
    // public static int wifiModulePort = 21567;
    public static String CMD = "";
    private EditText CustomOne, CustomTwo, CustomThree, CustomFour, CustomInput;
    private Button Custom_Accept, Custom_Start, home_btn;
    private String PlayerInputCount;
    private int PlayerCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_customized_screen);
        TypeCastView();
        Custom_Accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckPlayer();
            }
        });
        Custom_Start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCustomizedBoard();
                CMD = "Custom_Start";
                DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
                dataSendtoRasp.execute();
            }
        });
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CMD = "Home_Screen";
                DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
                dataSendtoRasp.execute();
                startActivity(new Intent(CustomizedScreen.this, MainActivity.class));
            }
        });
    }

    private void TypeCastView() {
        //////////////////EDITTEXT////////////////////////
        CustomOne = findViewById(R.id.tV_TeamTwo_Foul);
        CustomTwo = findViewById(R.id.TeamTwo);
        CustomThree = findViewById(R.id.TeamTwo);
        CustomFour = findViewById(R.id.PlayerFour);
        CustomInput = findViewById(R.id.Custom_Input);
        ////////////////////BUTTON///////////////////
        home_btn = findViewById(R.id.btn_home);
        Custom_Accept = findViewById(R.id.CustomAccept);
        Custom_Start = findViewById(R.id.CustomStart);

        CustomOne.addTextChangedListener(loginTextWatcher);
        CustomTwo.addTextChangedListener(loginTextWatcher);
        CustomThree.addTextChangedListener(loginTextWatcher);
        CustomFour.addTextChangedListener(loginTextWatcher);
        CustomInput.addTextChangedListener(Textwatcher);

    }

    private void CheckPlayer() {
        PlayerInputCount = CustomInput.getText().toString();
        PlayerCount = Integer.parseInt(PlayerInputCount);
        if (PlayerCount == 1) {
            CustomOne.setVisibility(View.VISIBLE);
            CustomInput.setText("");
        }
        if (PlayerCount == 2) {
            CustomOne.setVisibility(View.VISIBLE);
            CustomTwo.setVisibility(View.VISIBLE);
            CustomInput.setText("");
        }
        if (PlayerCount == 3) {
            CustomOne.setVisibility(View.VISIBLE);
            CustomThree.setVisibility(View.VISIBLE);
            CustomTwo.setVisibility(View.VISIBLE);
            CustomInput.setText("");
        }
        if (PlayerCount == 4) {
            CustomOne.setVisibility(View.VISIBLE);
            CustomTwo.setVisibility(View.VISIBLE);
            CustomThree.setVisibility(View.VISIBLE);
            CustomFour.setVisibility(View.VISIBLE);
            CustomInput.setText("");
        }

        if (PlayerCount > 4) {
            PlayerCount = 4;
            Toast.makeText(this, "ONLY 4 PLAYER/TEAM", Toast.LENGTH_LONG).show();
            CustomInput.setText("");
        }
        if (PlayerCount < 1) {
            PlayerCount = 0;
            Toast.makeText(this, "PLEASE INPUT A NUMBER", Toast.LENGTH_LONG).show();
            CustomInput.setText("");
        }
    }

    @Override
    public void onBackPressed() {
        // disable back button
    }

    public void openCustomizedBoard() {
        /////////////ASSIGN INPUTED NAMES TO STRING/////////////////
        String NameOne = CustomOne.getText().toString();
        String NameTwo = CustomTwo.getText().toString();
        String NameThree = CustomThree.getText().toString();
        String NameFour = CustomFour.getText().toString();
        Intent intent = new Intent(this, CustomizedBoard.class);
        intent.putExtra(DATAPLAYERONE, NameOne);
        intent.putExtra(DATAPLAYERTWO, NameTwo);
        intent.putExtra(DATAPLAYERTHREE, NameThree);
        intent.putExtra(DATAPLAYERFOUR, NameFour);
        startActivity(intent);
    }

    public class DataSendtoRasp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                InetAddress inetAddress = InetAddress.getByName(Constants.WIFI_MODULE_IP);
                socket = new java.net.Socket(inetAddress, Constants.WIFI_MODULE_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(CMD);
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String NameOne = CustomOne.getText().toString();
            String NameTwo = CustomTwo.getText().toString();
            String NameOneTwo = CustomThree.getText().toString();
            String NameTwoTwo = CustomFour.getText().toString();
            ///CHECK IF THE EDITTEXT HAS DATA ON IT
            Custom_Start.setEnabled(!NameOne.isEmpty() || !NameTwo.isEmpty() || !NameOneTwo.isEmpty()
                    || !NameTwoTwo.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private TextWatcher Textwatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String Input_Num = CustomInput.getText().toString();
            ///CHECK IF THE EDITTEXT HAS DATA ON IT
            Custom_Accept.setEnabled(!Input_Num.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
}
