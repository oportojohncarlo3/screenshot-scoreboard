package com.thesisforevery.screenshot.activity.basketball;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.thesisforevery.screenshot.R;
import com.thesisforevery.screenshot.activity.MainActivity;
import com.thesisforevery.screenshot.helper.Constants;
import com.thesisforevery.screenshot.helper.ObjectBox;
import com.thesisforevery.screenshot.helper.ScoredNetworkChangeReceiver;
import com.thesisforevery.screenshot.model.BasketballGame;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import io.objectbox.Box;

public class BasketBall_Board extends AppCompatActivity implements View.OnClickListener {

    Socket socket = null;
    public static String CMD = "";
    private int QuaterTime = 1;
    int blueTimeOutCount, redTimeOutCount = 1;
    private int ScoreTone, ScoreTTwo, FoulCntBlue, FoulCntRed;
    private AlertDialog dialog;
    private TextView TextONE, TextTWO;
    private TextView tVBlueScoreDisplay, tV_scoreDisTwo, tV_TeamOne_Foul, tV_TimeOut_Blue;
    private TextView tV_QuaterCnt, tV_ShotTme, tV_TeamTwo_Foul, tV_TimeOut_Red;
    private Button btnPrevious, btnHome;
    private EditText eTGameTime;
    private Button btnStartGameTime, btnInputGameTime, btnStopGameTime, btnShotClock, btnClear;
    //             PlsOneTone,       PlsTwoTone,       PlsThreeTone,       TimeoutBluebtn, FoulBluebtn;
    private Button btnPlusOneToBlue, btnPlusTwoToBlue, btnPlusThreeToBlue, btnTimeOutBlue, btnFoulBlue;
    //             PlsOneTtwo,       PlstwoTtwo,     PlsthreeTtwo,      TimeoutRedbtn, FoulRedbtn;
    private Button btnPlusOneToRed, btnPlusTwoToRed, btnPlusThreeToRed, btnTimeOutRed, btnFouldRed;
    private ScoredNetworkChangeReceiver scoredNetworkChangeReceiver;
    boolean enableButtons;

    Box<BasketballGame> basketballGameBox;
    BasketballGame basketballGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_basketball_board);
        DeclaredViews();
        DeclaredButton();

        scoredNetworkChangeReceiver = new ScoredNetworkChangeReceiver(new ScoredNetworkChangeReceiver.OnConnectionChangeListener() {
            @Override
            public void onConnected() {
                Toast.makeText(BasketBall_Board.this, "CONNECTED", Toast.LENGTH_SHORT).show();
                enableButtons = true;
                isToEnableButtons();
            }

            @Override
            public void onDisConnected() {
                Toast.makeText(BasketBall_Board.this, "DISCONNECTED", Toast.LENGTH_SHORT).show();
                enableButtons = false;
                isToEnableButtons();
            }
        });
        // Create an IntentFilter instance.
        IntentFilter intentFilter = new IntentFilter();
        // Add network connectivity change action.
        intentFilter.addAction(Constants.CONNECTIVITY_INTENT_FILTER);
        // Set broadcast receiver priority.
        intentFilter.setPriority(100);
        registerReceiver(scoredNetworkChangeReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // If the broadcast receiver is not null then unregister it.
        // This action is better placed in activity onDestroy() method.
        if (this.scoredNetworkChangeReceiver != null) {
            unregisterReceiver(this.scoredNetworkChangeReceiver);
        }
    }

    public void isToEnableButtons() {
        //////////BUTTON SET TO BE CLICKABLE//////////////
        btnStartGameTime.setEnabled(enableButtons);
        btnInputGameTime.setEnabled(enableButtons);
        btnStopGameTime.setEnabled(enableButtons);
        btnClear.setEnabled(enableButtons);
        //////////TEAM BLUE/////////////////////////////
        btnPlusOneToBlue.setEnabled(enableButtons);
        btnPlusTwoToBlue.setEnabled(enableButtons);
        btnPlusThreeToBlue.setEnabled(enableButtons);
        btnTimeOutBlue.setEnabled(enableButtons);
        btnFoulBlue.setEnabled(enableButtons);
        ////////////////TEAM RED////////////////////////
        btnPlusOneToRed.setEnabled(enableButtons);
        btnPlusTwoToRed.setEnabled(enableButtons);
        btnPlusThreeToRed.setEnabled(enableButtons);
        btnTimeOutRed.setEnabled(enableButtons);
        btnFouldRed.setEnabled(enableButtons);
        //////////////////BUTTON TOOLS////////////////
        btnPrevious.setEnabled(enableButtons);
        btnHome.setEnabled(enableButtons);
    }

    public void DeclaredViews() {
        Intent intent = getIntent();
        String NameOne = intent.getStringExtra(BasketScreen.BAS_DATAONE);
        String NameTwo = intent.getStringExtra(BasketScreen.BAS_DATATWO);
        TextONE = findViewById(R.id.BasTeamOne);
        TextTWO = findViewById(R.id.BasTeamTwo);
        TextONE.setText(NameOne);
        TextTWO.setText(NameTwo);

        // TODO: START Don't Delete
        basketballGameBox = ObjectBox.get().boxFor(BasketballGame.class);

        Log.d("OBJECTBOX", "" + basketballGameBox.getAll());

        basketballGameBox.removeAll();

        basketballGame = new BasketballGame();
        basketballGame.setTeam1Name(NameOne);
        basketballGame.setTeam2Name(NameTwo);

        long gameId = basketballGameBox.put(basketballGame);
        basketballGame.setId(gameId);
        basketballGameBox.put(basketballGame);

        Log.d("OBJECTBOX", "" + basketballGameBox.getAll());
        // TODO: END Don't Delete

 /*
    Text Display for Game Time, Shot clock, Team Foul Left
  */
        tV_QuaterCnt = findViewById(R.id.tV_QuaterCnt);
        tV_ShotTme = findViewById(R.id.tV_ShotTme);
        tV_TeamTwo_Foul = findViewById(R.id.tV_TeamTwo_Foul);
        tV_TimeOut_Red = findViewById(R.id.tV_TimeOut_Red);
    }

    public void DeclaredButton() {
        /////////////BUTTON FOR TOOLS//////////////
        btnPrevious = findViewById(R.id.previous);
        btnHome = findViewById(R.id.btn_home);
        // EDITTEXT USED FOR INPUTTING THE GAME TIME
        eTGameTime = findViewById(R.id.Gametime_input);
        ///////////////////BUTTONS USE FOR GAME TIME////////////////////
        btnStartGameTime = findViewById(R.id.Start_GameTime);
        btnInputGameTime = findViewById(R.id.Input_GTime);
        btnStopGameTime = findViewById(R.id.stop_GameTime);
        btnClear = findViewById(R.id.BtnClear);
        btnShotClock = findViewById(R.id.shotclock_btn);
        ////////////////////BUTTONS FOR TEAM BLUE ///////////////////
        btnPlusOneToBlue = findViewById(R.id.PlsOneTone);
        btnPlusTwoToBlue = findViewById(R.id.PlsTwoTone);
        btnPlusThreeToBlue = findViewById(R.id.PlsThreeTone);
        btnTimeOutBlue = findViewById(R.id.TimeoutBluebtn);
        btnFoulBlue = findViewById(R.id.FoulBluebtn);
        ///////////////////BUTTON FOR TEAM RED//////////////////////
        btnPlusOneToRed = findViewById(R.id.PlsOneTtwo);
        btnPlusTwoToRed = findViewById(R.id.PlstwoTtwo);
        btnPlusThreeToRed = findViewById(R.id.PlsthreeTtwo);
        btnTimeOutRed = findViewById(R.id.TimeoutRedbtn);
        btnFouldRed = findViewById(R.id.FoulRedbtn);
        //////////BUTTON SET TO BE CLICKABLE//////////////
        btnStartGameTime.setOnClickListener(this);
        btnInputGameTime.setOnClickListener(this);
        btnStopGameTime.setOnClickListener(this);
        btnClear.setOnClickListener(this);
        //////////TEAM BLUE/////////////////////////////
        btnPlusOneToBlue.setOnClickListener(this);
        btnPlusTwoToBlue.setOnClickListener(this);
        btnPlusThreeToBlue.setOnClickListener(this);
        btnTimeOutBlue.setOnClickListener(this);
        btnFoulBlue.setOnClickListener(this);
        ////////////////TEAM RED////////////////////////
        btnPlusOneToRed.setOnClickListener(this);
        btnPlusTwoToRed.setOnClickListener(this);
        btnPlusThreeToRed.setOnClickListener(this);
        btnTimeOutRed.setOnClickListener(this);
        btnFouldRed.setOnClickListener(this);
        //////////////////BUTTON TOOLS////////////////
        btnPrevious.setOnClickListener(this);
        btnHome.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Start_GameTime:
                btnShotClock.setEnabled(true);
                btnPlusOneToBlue.setEnabled(true);
                btnPlusTwoToBlue.setEnabled(true);
                btnPlusThreeToBlue.setEnabled(true);
                btnTimeOutBlue.setEnabled(true);
                btnFoulBlue.setEnabled(true);
                btnPlusOneToRed.setEnabled(true);
                btnPlusTwoToRed.setEnabled(true);
                btnPlusThreeToRed.setEnabled(true);
                btnTimeOutRed.setEnabled(true);
                btnFouldRed.setEnabled(true);
                btnClear.setEnabled(true);
                break;
            case R.id.Input_GTime:

                break;
            case R.id.stop_GameTime:

                break;

            case R.id.BtnClear:
                btnShotClock.setEnabled(false);
                btnClear.setEnabled(false);
                btnPlusOneToBlue.setEnabled(false);
                btnPlusTwoToBlue.setEnabled(false);
                btnPlusThreeToBlue.setEnabled(false);
                btnTimeOutBlue.setEnabled(false);
                btnFoulBlue.setEnabled(false);
                btnPlusOneToRed.setEnabled(false);
                btnPlusTwoToRed.setEnabled(false);
                btnPlusThreeToRed.setEnabled(false);
                btnTimeOutRed.setEnabled(false);
                btnFouldRed.setEnabled(false);
                btnClear.setEnabled(false);

                break;


            case R.id.previous:
                dialog = new AlertDialog.Builder(this).create();
                dialog.setTitle("RENAME TEAM?");
                dialog.setMessage("DATA WILL BE LOST");
                dialog.setButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Previous();
                    }
                });
                dialog.setButton2("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.btn_home:
                dialog = new AlertDialog.Builder(this).create();
                dialog.setTitle("Back to MAIN SCREEN");
                dialog.setMessage("You sure?");
                dialog.setButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainMenu();
                    }
                });
                dialog.setButton2("NO,", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

                break;
        }
    }

    public class DataSendtoRasp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                InetAddress inetAddress = InetAddress.getByName(Constants.WIFI_MODULE_IP);
                socket = new java.net.Socket(inetAddress, Constants.WIFI_MODULE_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(CMD);
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void Previous() {
        startActivity(new Intent(BasketBall_Board.this, BasketScreen.class));
    }

    @Override
    public void onBackPressed() {
        //disable back button
    }

    public void MainMenu() {
        CMD = "MainMenu";
        DataSendtoRasp MainMenu = new DataSendtoRasp();
        MainMenu.execute();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}
