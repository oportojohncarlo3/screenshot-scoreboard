package com.thesisforevery.screenshot.activity.badminton;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.thesisforevery.screenshot.R;
import com.thesisforevery.screenshot.helper.Constants;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;


public class BadminScreen extends AppCompatActivity{
    public static final String DATAONE ="DATAONE";
    public static final String DATATWO ="DATATWO";
    public static final String DATATHREE ="DATATHREE";
    public static final String DATAFOUR="DATAFOUR";
    Socket socket = null;
    public static String CMD = "";
    private String NameOne,NameTwo,NameThree,NameFour;
    private Button VacceptBtn,home_btn;
   private EditText PLayerOne,PLayerTwo,PLayerThree,PLayerFour;
    private ToggleButton btn_FourPlayer;
    private Handler mHandler = new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_badmin_screen);
        ViewDeclaration();


        VacceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PlayerOneData();
                mHandler.postDelayed(PlayerTwoData,800);
                mHandler.postDelayed(PlayerThreeData,1200);
                mHandler.postDelayed(PlayerFourData,1500);

            }
        });
 ////////////BUTTON TO ALLOW USER TO CHOICE 1v1 or 2v2///////////
        btn_FourPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btn_FourPlayer.isChecked()) {
                    PLayerThree.setVisibility(View.VISIBLE);
                    PLayerFour.setVisibility(View.VISIBLE);
                    PLayerThree.addTextChangedListener(loginTextWatcher);
                    PLayerFour.addTextChangedListener(loginTextWatcher);
                    btn_FourPlayer.setChecked(true);
                } else if (!btn_FourPlayer.isChecked()) {
                    PLayerThree.setVisibility(View.GONE);
                    PLayerFour.setVisibility(View.GONE);
                    btn_FourPlayer.setChecked(false);
                }
            }
        });
    }
    public void ViewDeclaration(){
 /////////////////TEXTVIEW /////////////////////////
        PLayerOne = findViewById(R.id.tV_TeamTwo_Foul);
        PLayerTwo = findViewById(R.id.TeamTwo);
        PLayerThree = findViewById(R.id.PlayerThree);
        PLayerFour = findViewById(R.id.PlayerFour);
        PLayerOne.addTextChangedListener(loginTextWatcher);
        PLayerTwo.addTextChangedListener(loginTextWatcher);
 ////////////////BUTTON///////////////////////
        btn_FourPlayer = findViewById(R.id.btn_FourPlayer);
        home_btn = findViewById(R.id.btn_home);
        VacceptBtn = findViewById(R.id.VacceptBtn);
    }
    public void openBadmintonBoard(){
        /*
        Saves inputted names to a string and will be transferred to Badminton ScoreBoard UI
         */
        NameOne = PLayerOne.getText().toString();
        NameTwo = PLayerTwo.getText().toString();
        NameThree = PLayerThree.getText().toString();
        NameFour = PLayerFour.getText().toString();
        Intent intent = new Intent(this,Badminton_Board.class);
        intent.putExtra(DATAONE, NameOne);
        intent.putExtra(DATATWO, NameTwo);
        intent.putExtra(DATATHREE, NameThree);
        intent.putExtra(DATAFOUR, NameFour);
        startActivity(intent);
    }

    public class DataSendtoRasp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                InetAddress inetAddress = InetAddress.getByName(Constants.WIFI_MODULE_IP);
                socket = new java.net.Socket(inetAddress, Constants.WIFI_MODULE_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(CMD);
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
 ///////////////CHECKS IF THE USER INPUTTED PLAYERS NAME////////////

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String NameOne = PLayerOne.getText().toString();
            String NameTwo = PLayerTwo.getText().toString();
            ///CHECK IF THE EDITTEXT HAS DATA ON IT
            VacceptBtn.setEnabled(!NameOne.isEmpty() && !NameTwo.isEmpty());
        }
        @Override
        public void afterTextChanged(Editable s) {
        }
    };

//////SENDS PLAYER ONE NAME///////////
    public void PlayerOneData()
    {
        NameOne = PLayerOne.getText().toString();
        CMD = "bd_p1_name" + NameOne;
       DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
        dataSendtoRasp.execute();
    }
//////SENDS PLAYER TWO NAME///////////
    private Runnable PlayerTwoData  = new Runnable () {
        @Override
        public void run() {
            NameTwo = PLayerTwo.getText().toString();
            CMD = "bd_p2_name" + NameTwo;
            DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
            dataSendtoRasp.execute();
            Toast.makeText(BadminScreen.this, "SYNCHRONIZING...", Toast.LENGTH_SHORT).show();
        }
    };
 //////SENDS PLAYER THREE NAME///////////
    private Runnable PlayerThreeData = new Runnable() {
        @Override
        public void run() {
            NameThree = PLayerThree.getText().toString();
            CMD = "bd_p3_name" + NameThree;
            DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
            dataSendtoRasp.execute();
            Toast.makeText(BadminScreen.this, "ALMOST DONE", Toast.LENGTH_SHORT).show();
        }
    };
//////SENDS PLAYER FOUR NAME///////////
    private Runnable PlayerFourData = new Runnable() {
        @Override
        public void run() {
            NameFour = PLayerFour.getText().toString();
            CMD = "bd_p4_name" + NameFour;
            DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
            dataSendtoRasp.execute();
            Toast.makeText(BadminScreen.this, "LET THE GAME BEGINS", Toast.LENGTH_LONG).show();
            openBadmintonBoard();
        }
    };
    }

