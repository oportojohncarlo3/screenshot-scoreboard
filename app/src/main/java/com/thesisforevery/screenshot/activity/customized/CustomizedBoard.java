package com.thesisforevery.screenshot.activity.customized;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thesisforevery.screenshot.R;
import com.thesisforevery.screenshot.helper.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class CustomizedBoard extends AppCompatActivity implements View.OnDragListener, View.OnLongClickListener, View.OnClickListener {
    Socket socket = null;
    public static String CMD = "";
    public int ScoreOne, ScoreTwo, ScoreThree, ScoreFour, Scorenum;
    private TextView DisCustOne, DisCustTwo, DisCustThree, DisCustFour, text_versus;
    private TextView DisCustScoreOne, DisCustScoreTwo, DisCustScoreThree, DisCustScoreFour, numDisplay;
    private LinearLayout dragScoreBlue, dragScoreRed, dragScoreGreen, dragScoreYellow;
    private LinearLayout LayoutDisCustOne, LayoutDisCustTwo, LayoutDisCustThree, LayoutDisFour, LayoutGameTime, NumDis;
    private Button addScoreCustomOne, minScoreCustomOne, addScoreCustomTwo, addScoreCustomFive, minScoreCustomFive,
            minScoreCustomTwo, addScoreCustomThree, minScoreCustomThree, addScoreCustomFour, minScoreCustomFour, btn_clear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_customized_board);
        Intent intent = getIntent();
        String NameOne = intent.getStringExtra(CustomizedScreen.DATAPLAYERONE);
        String NameTwo = intent.getStringExtra(CustomizedScreen.DATAPLAYERTWO);
        String NameThree = intent.getStringExtra(CustomizedScreen.DATAPLAYERTHREE);
        String NameFour = intent.getStringExtra(CustomizedScreen.DATAPLAYERFOUR);
        DisCustOne = findViewById(R.id.DisCustOne);
        DisCustTwo = findViewById(R.id.DisCustTwo);
        DisCustThree = findViewById(R.id.DisCustThree);
        DisCustFour = findViewById(R.id.DisCustFour);
        text_versus = findViewById(R.id.text_versus);
        numDisplay = findViewById(R.id.numDisplay);
        DisCustOne.setText(NameOne);
        DisCustTwo.setText(NameTwo);
        DisCustThree.setText(NameThree);
        DisCustFour.setText(NameFour);
        NumDis = findViewById(R.id.NumDis);
        LayoutDisCustOne = findViewById(R.id.LayoutDisCustOne);
        LayoutDisCustTwo = findViewById(R.id.LayoutDisCustTwo);
        LayoutDisCustThree = findViewById(R.id.LayoutDisCustThree);
        LayoutDisFour = findViewById(R.id.LayoutDisFour);
        LayoutGameTime = findViewById(R.id.LayoutGameTime);
        dragScoreBlue = findViewById(R.id.dragScoreBlue);
        dragScoreRed = findViewById(R.id.dragScoreRed);
        dragScoreGreen = findViewById(R.id.dragScoreGreen);
        dragScoreYellow = findViewById(R.id.dragScoreYellow);


        DisCustScoreOne = findViewById(R.id.DisCustScoreOne);
        DisCustScoreTwo = findViewById(R.id.DisCustScoreTwo);
        DisCustScoreThree = findViewById(R.id.DisCustScoreThree);
        DisCustScoreFour = findViewById(R.id.DisCustScoreFour);
        numDisplay = findViewById(R.id.numDisplay);

        ///////////////////////BUTTON////////////////////
        addScoreCustomOne = findViewById(R.id.addScoreCustomOne);
        minScoreCustomOne = findViewById(R.id.minScoreCustomOne);
        addScoreCustomTwo = findViewById(R.id.addScoreCustomTwo);
        minScoreCustomTwo = findViewById(R.id.minScoreCustomTwo);
        addScoreCustomThree = findViewById(R.id.addScoreCustomThree);
        minScoreCustomThree = findViewById(R.id.minScoreCustomThree);
        addScoreCustomFour = findViewById(R.id.addScoreCustomFour);
        minScoreCustomFour = findViewById(R.id.minScoreCustomFour);
        addScoreCustomFive = findViewById(R.id.addNumDis);
        minScoreCustomFive = findViewById(R.id.minNumDis);
        btn_clear = findViewById(R.id.btn_clear);

        dragScoreBlue.setTag("dragScoreBlue");
        dragScoreRed.setTag("dragScoreRed");
        dragScoreGreen.setTag("dragScoreGreen");
        dragScoreYellow.setTag("dragScoreYellow");
        LayoutGameTime.setTag("LayoutGameTime");
        LayoutDisCustOne.setTag("LayoutDisCustOne");
        LayoutDisCustTwo.setTag("LayoutDisCustTwo");
        LayoutDisCustThree.setTag("LayoutDisCustThree");
        LayoutDisFour.setTag("LayoutDisFour");
        text_versus.setTag("text_versus");
        NumDis.setTag("NumDis");
        btn_clear.setTag("btn_clear");

        NumDis.setOnLongClickListener(this);
        text_versus.setOnLongClickListener(this);
        LayoutDisCustOne.setOnLongClickListener(this);
        LayoutDisCustTwo.setOnLongClickListener(this);
        LayoutDisCustThree.setOnLongClickListener(this);
        LayoutDisFour.setOnLongClickListener(this);
        LayoutGameTime.setOnLongClickListener(this);
        dragScoreBlue.setOnLongClickListener(this);
        dragScoreRed.setOnLongClickListener(this);
        dragScoreGreen.setOnLongClickListener(this);
        dragScoreYellow.setOnLongClickListener(this);
        btn_clear.setOnLongClickListener(this);

        //Set Drag Event Listeners for defined layouts
        findViewById(R.id.layout1).setOnDragListener(this);
        findViewById(R.id.layout2).setOnDragListener(this);
        findViewById(R.id.layout3).setOnDragListener(this);
        findViewById(R.id.layout4).setOnDragListener(this);
        findViewById(R.id.layout5).setOnDragListener(this);
        findViewById(R.id.layout6).setOnDragListener(this);
        findViewById(R.id.layout7).setOnDragListener(this);
        findViewById(R.id.layout8).setOnDragListener(this);
        findViewById(R.id.layout9).setOnDragListener(this);
        findViewById(R.id.layout10).setOnDragListener(this);
        findViewById(R.id.layout11).setOnDragListener(this);
        findViewById(R.id.layout12).setOnDragListener(this);
        findViewById(R.id.layout13).setOnDragListener(this);
        findViewById(R.id.layout14).setOnDragListener(this);
        findViewById(R.id.layout15).setOnDragListener(this);
        findViewById(R.id.selectionscore).setOnDragListener(this);

        //////////////////////////////////////////////////
        addScoreCustomOne.setOnClickListener(this);
        minScoreCustomOne.setOnClickListener(this);
        addScoreCustomTwo.setOnClickListener(this);
        minScoreCustomTwo.setOnClickListener(this);
        addScoreCustomThree.setOnClickListener(this);
        minScoreCustomThree.setOnClickListener(this);
        addScoreCustomFour.setOnClickListener(this);
        minScoreCustomFour.setOnClickListener(this);
        addScoreCustomFive.setOnClickListener(this);
        minScoreCustomFive.setOnClickListener(this);
    }

    @Override
    public boolean onLongClick(View v) {
        // Create a new ClipData.Item from the ImageView object's tag
        ClipData.Item item = new ClipData.Item((CharSequence) v.getTag());
        // Create a new ClipData using the tag as a label, the plain text MIME type, and
        // the already-created item. This will create a new ClipDescription object within the
        // ClipData, and set its MIME type entry to "text/plain"
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
        ClipData data = new ClipData(v.getTag().toString(), mimeTypes, item);
        // Instantiates the drag shadow builder.
        View.DragShadowBuilder dragshadow = new View.DragShadowBuilder(v);
        // Starts the drag
        v.startDrag(data        // data to be dragged
                , dragshadow   // drag shadow builder
                , v           // local data about the drag and drop operation
                , 0          // flags (not currently used, set to 0)
        );
        return true;
    }

    // This is the method that the system calls when it dispatches a drag event to the listener.
    @Override
    public boolean onDrag(View v, DragEvent event) {
        // Defines a variable to store the action type for the incoming event
        int action = event.getAction();
        // Handles each of the expected events
        switch (action) {

            case DragEvent.ACTION_DRAG_STARTED:
                // Determines if this View can accept the dragged data
                if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    // if you want to apply color when drag started to your view you can uncomment below lines
                    // to give any color tint to the View to indicate that it can accept data.
                    // v.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
                    // Invalidate the view to force a redraw in the new tint
                    //  v.invalidate();
                    // returns true to indicate that the View can accept the dragged data.
                    return true;
                }
                // Returns false. During the current drag and drop operation, this View will
                // not receive events again until ACTION_DRAG_ENDED is sent.
                return false;

            case DragEvent.ACTION_DRAG_ENTERED:

                LinearLayout containers = (LinearLayout) v;

                if (containers.getChildCount() == 0) {
                    // Applies a GRAY or any color tint to the View. Return true; the return value is ignored.
                    v.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);
                    // Invalidate the view to force a redraw in the new tint
                    v.invalidate();
                    return true;
                } else {
                    return false;
                }

            case DragEvent.ACTION_DRAG_LOCATION:
                // Ignore the event
                return true;

            case DragEvent.ACTION_DRAG_EXITED:
                // Re-sets the color tint to blue. Returns true; the return value is ignored.
                // view.getBackground().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
                //It will clear a color filter .
                v.getBackground().clearColorFilter();
                // Invalidate the view to force a redraw in the new tint
                v.invalidate();
                return true;

            case DragEvent.ACTION_DROP:
                // Gets the item containing the dragged data
                ClipData.Item item = event.getClipData().getItemAt(0);
                // Gets the text data from the item.
                String dragData = item.getText().toString();
                // Displays a message containing the dragged data.
                // Toast.makeText(this, "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();
                // Turns off any color tints
                v.getBackground().clearColorFilter();
                // Invalidates the view to force a redraw
                v.invalidate();

                //caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                LinearLayout container = (LinearLayout) v;

                if (container.getChildCount() == 0) {
                    View draggedView = (View) event.getLocalState();
                    ViewGroup owner = (ViewGroup) draggedView.getParent();
                    owner.removeView(draggedView); //remove the dragged view from the menu

                    container.addView(draggedView); //Add the dragged view
                    draggedView.setVisibility(View.VISIBLE); //finally set Visibility to VISIBLE

                    try {
                        updateRaspDisplay(container.getTag().toString(), draggedView.getTag().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    return true;
                } else {
                    return false;
                }

            case DragEvent.ACTION_DRAG_ENDED:
                // Turns off any color tinting
                v.getBackground().clearColorFilter();
                // Invalidates the view to force a redraw
                v.invalidate();
                // Does a getResult(), and displays what happened.
                return event.getResult();
            default:
                Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                break;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addScoreCustomOne:
                ScoreOne++;
                CMD = "add_scoreOne";
                DataSendtoRasp add_scoreOne = new DataSendtoRasp();
                add_scoreOne.execute();
                if (ScoreOne == 99) {
                    ScoreOne = 99;
                }
                DisCustScoreOne.setText(ScoreOne + "");
                break;
            case R.id.minScoreCustomOne:
                ScoreOne--;
                CMD = "min_scoreOne";
                DataSendtoRasp min_scoreOne = new DataSendtoRasp();
                min_scoreOne.execute();
                if (ScoreOne < 1) {
                    ScoreOne = 0;
                }
                DisCustScoreOne.setText(ScoreOne + "");
                break;

            case R.id.addScoreCustomTwo:
                ScoreTwo++;
                CMD = "add_scoreTwo";
                DataSendtoRasp add_scoreTwo = new DataSendtoRasp();
                add_scoreTwo.execute();
                if (ScoreTwo == 99) {
                    ScoreTwo = 99;

                }
                DisCustScoreTwo.setText(ScoreTwo + "");
                break;
            case R.id.minScoreCustomTwo:
                ScoreTwo--;
                CMD = "min_scoreTwo";
                DataSendtoRasp min_scoreTwo = new DataSendtoRasp();
                min_scoreTwo.execute();
                if (ScoreTwo < 1) {
                    ScoreTwo = 0;
                }
                DisCustScoreTwo.setText(ScoreTwo + "");

                break;
            case R.id.addScoreCustomThree:
                ScoreThree++;
                CMD = "add_scoreThree";
                DataSendtoRasp add_scoreThree = new DataSendtoRasp();
                add_scoreThree.execute();
                if (ScoreThree == 99) {
                    ScoreThree = 99;
                }
                DisCustScoreThree.setText(ScoreThree + "");
                break;
            case R.id.minScoreCustomThree:
                ScoreThree--;
                CMD = "min_scoreThree";
                DataSendtoRasp min_scoreThree = new DataSendtoRasp();
                min_scoreThree.execute();
                if (ScoreThree < 1) {
                    ScoreThree = 0;
                }
                DisCustScoreThree.setText(ScoreThree + "");
                break;
            case R.id.addScoreCustomFour:
                ScoreFour++;
                CMD = "add_scoreFour";
                DataSendtoRasp add_scoreFour = new DataSendtoRasp();
                add_scoreFour.execute();
                if (ScoreFour == 99) {
                    ScoreFour = 99;
                }
                DisCustScoreFour.setText(ScoreFour + "");
                break;
            case R.id.minScoreCustomFour:
                ScoreFour--;
                CMD = "min_scoreFour";
                DataSendtoRasp min_scoreFour = new DataSendtoRasp();
                min_scoreFour.execute();
                if (ScoreFour < 1) {
                    ScoreFour = 0;
                }
                DisCustScoreFour.setText(ScoreFour + "");
                break;

            case R.id.addNumDis:
                Scorenum++;
                CMD = "num_Addscore";
                DataSendtoRasp num_Addscore = new DataSendtoRasp();
                num_Addscore.execute();
                if (Scorenum == 99) {
                    Scorenum = 99;
                }
                numDisplay.setText(Scorenum + "");
                break;

            case R.id.minNumDis:
                Scorenum--;
                CMD = "num_Minscore";
                DataSendtoRasp num_Minscore = new DataSendtoRasp();
                num_Minscore.execute();
                if (Scorenum < 1) {
                    Scorenum = 0;
                }
                numDisplay.setText(Scorenum + "");
                break;
        }
    }

    private void updateRaspDisplay(String parentId, String childId) throws JSONException {
        JSONObject jsonParam = new JSONObject();
        jsonParam.put("parent_id", parentId);
        jsonParam.put("child_id", childId);
        CMD = jsonParam.toString();
        Log.d("DataSendtoRasp", CMD);
        new DataSendtoRasp().execute();
    }

    public class DataSendtoRasp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                InetAddress inetAddress = InetAddress.getByName(Constants.WIFI_MODULE_IP);
                socket = new java.net.Socket(inetAddress, Constants.WIFI_MODULE_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(CMD);
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}