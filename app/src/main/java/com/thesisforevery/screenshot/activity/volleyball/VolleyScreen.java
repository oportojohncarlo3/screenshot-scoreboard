package com.thesisforevery.screenshot.activity.volleyball;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.thesisforevery.screenshot.R;
import com.thesisforevery.screenshot.activity.MainActivity;
import com.thesisforevery.screenshot.helper.Constants;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class VolleyScreen extends AppCompatActivity {
    public static final String DATAONE = "DATAONE";
    public static final String DATATWO = "DATATWO";
    Socket socket = null;
    public static String CMD = "";
    private EditText TeamOne, TeamTwo;
    private Button VollyAccept, home_btn;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_volley_screen);
        initTypeCast();
        VollyAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TeamOneData();
                mHandler.postDelayed(TeamTwoData, 1000);
            }
        });

        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CMD = "MainMenu";
                DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
                dataSendtoRasp.execute();
                startActivity(new Intent(VolleyScreen.this, MainActivity.class));
            }
        });
    }

    public void TeamOneData() {
        String NameOne = TeamOne.getText().toString();
        CMD = "vb_p1_name" + NameOne;
        DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
        dataSendtoRasp.execute();
    }

    private Runnable TeamTwoData = new Runnable() {
        @Override
        public void run() {
            String NameTwo = TeamTwo.getText().toString();
            CMD = "vb_p2_name" + NameTwo;
            DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
            dataSendtoRasp.execute();
            openVolleyScoreBoard();
        }
    };

    @Override
    public void onBackPressed() {
        //disable back button
    }

    public void initTypeCast() {
        home_btn = findViewById(R.id.btn_home);
        TeamOne = findViewById(R.id.tV_TeamTwo_Foul);
        TeamTwo = findViewById(R.id.TeamTwo);
        TeamOne.addTextChangedListener(loginTextWatcher);
        TeamTwo.addTextChangedListener(loginTextWatcher);
        VollyAccept = findViewById(R.id.VacceptBtn);
    }

    public class DataSendtoRasp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                InetAddress inetAddress = InetAddress.getByName(Constants.WIFI_MODULE_IP);
                socket = new java.net.Socket(inetAddress, Constants.WIFI_MODULE_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(CMD);
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public void openVolleyScoreBoard() {

        /* para ma save ang data og ma display sa VolleyScoreBoard */
        String NameOne = TeamOne.getText().toString();
        String NameTwo = TeamTwo.getText().toString();
        Intent intent = new Intent(this, VolleyBall_Board.class);
        intent.putExtra(DATAONE, NameOne);
        intent.putExtra(DATATWO, NameTwo);
        startActivity(intent);
    }

    private TextWatcher loginTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String NameOne = TeamOne.getText().toString();
            String NameTwo = TeamTwo.getText().toString();
            ///CHECK IF THE EDITTEXT HAS DATA ON IT
            VollyAccept.setEnabled(!NameOne.isEmpty() && !NameTwo.isEmpty());
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

}
