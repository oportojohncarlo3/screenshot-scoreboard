package com.thesisforevery.screenshot.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.thesisforevery.screenshot.R;
import com.thesisforevery.screenshot.activity.badminton.BadminScreen;
import com.thesisforevery.screenshot.activity.basketball.BasketScreen;
import com.thesisforevery.screenshot.activity.customized.CustomizedScreen;
import com.thesisforevery.screenshot.activity.volleyball.VolleyScreen;
import com.thesisforevery.screenshot.helper.Constants;
import com.thesisforevery.screenshot.helper.ObjectBox;
import com.thesisforevery.screenshot.helper.ScoredNetworkChangeReceiver;
import com.thesisforevery.screenshot.model.Game;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import io.objectbox.Box;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Socket socket = null;
    public static String CMD = "";
    Button Volley, Basket, Badminton, Tennis, Customized;
    private AlertDialog dialog;
    private ScoredNetworkChangeReceiver scoredNetworkChangeReceiver;
    boolean enableButtons;

    @Override
    public void onBackPressed() {
        //disable back button
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        TypeCastView();

        scoredNetworkChangeReceiver = new ScoredNetworkChangeReceiver(new ScoredNetworkChangeReceiver.OnConnectionChangeListener() {
            @Override
            public void onConnected() {
                Toast.makeText(MainActivity.this, "CONNECTED", Toast.LENGTH_SHORT).show();
                enableButtons = true;
                isToEnableButtons();
            }
            @Override
            public void onDisConnected() {
                Toast.makeText(MainActivity.this, "DISCONNECTED", Toast.LENGTH_SHORT).show();
                enableButtons = false;
                isToEnableButtons();
            }
        });

        // Create an IntentFilter instance.
        IntentFilter intentFilter = new IntentFilter();
        // Add network connectivity change action.
        intentFilter.addAction(Constants.CONNECTIVITY_INTENT_FILTER);
        // Set broadcast receiver priority.
        intentFilter.setPriority(100);
        registerReceiver(scoredNetworkChangeReceiver, intentFilter);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // If the broadcast receiver is not null then unregister it.
        // This action is better placed in activity onDestroy() method.
        if (this.scoredNetworkChangeReceiver != null) {
            unregisterReceiver(this.scoredNetworkChangeReceiver);
        }
    }

    public void TypeCastView() {
        Volley = findViewById(R.id.Volley);
        Basket = findViewById(R.id.Basket);
        Badminton = findViewById(R.id.Badminton);
        Tennis = findViewById(R.id.Tennis);
        Customized = findViewById(R.id.SCORED);
        Customized.setOnClickListener(this);
        Volley.setOnClickListener(this);
        Basket.setOnClickListener(this);
        Badminton.setOnClickListener(this);
        Tennis.setOnClickListener(this);
    }

    public void isToEnableButtons() {
        Volley.setEnabled(enableButtons);
        Basket.setEnabled(enableButtons);
        Badminton.setEnabled(enableButtons);
        Tennis.setEnabled(enableButtons);
        Customized.setEnabled(enableButtons);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Basket:
                CMD = "basketball";
                DataSendtoRasp basketball = new DataSendtoRasp();
                basketball.execute();
                dialog = new AlertDialog.Builder(this).create();
                dialog.setTitle("BASKETBALL");
                dialog.setMessage("NEW GAME or OPEN FILE");
                dialog.setButton("NEW GAME", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openBasketScreen();
                    }
                });
                dialog.setButton2("OPEN GAME FILE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                dialog.setButton3("BACK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.Volley:
                CMD = "volleyball";
                DataSendtoRasp volleyball = new DataSendtoRasp();
                volleyball.execute();
                dialog = new AlertDialog.Builder(this).create();
                dialog.setTitle("VOLLEYBALL");
                dialog.setMessage("NEW GAME or OPEN FILE");
                dialog.setButton("NEW GAME", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openVolleyScreen();
                    }
                });
                dialog.setButton2("OPEN GAME FILE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dialog.setButton3("BACK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.Badminton:
                CMD = "badminton";
                DataSendtoRasp badminton = new DataSendtoRasp();
                badminton.execute();
                dialog = new AlertDialog.Builder(this).create();
                dialog.setTitle("BADMINTON");
                dialog.setMessage("NEW GAME or OPEN FILE");
                dialog.setButton("NEW GAME", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openBadmintonScreen();
                    }
                });
                dialog.setButton2("OPEN GAME FILE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dialog.setButton3("BACK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
                break;
            case R.id.SCORED:
                CMD = "customized";
                DataSendtoRasp dataSendtoRasp = new DataSendtoRasp();
                dataSendtoRasp.execute();
                openCustomized();
                break;



        }
    }

    public class DataSendtoRasp extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                InetAddress inetAddress = InetAddress.getByName(Constants.WIFI_MODULE_IP);
                socket = new java.net.Socket(inetAddress, Constants.WIFI_MODULE_PORT);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(CMD);
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
    public void openVolleyScreen() {
        Intent intent = new Intent(this, VolleyScreen.class);
        startActivity(intent);
    }
    public void openBasketScreen() {
        Intent intent = new Intent(this, BasketScreen.class);
        startActivity(intent);
    }
    public void openBadmintonScreen() {
        Intent intent = new Intent(this, BadminScreen.class);
        startActivity(intent);
    }
    public void openCustomized() {
        Intent intent = new Intent(this, CustomizedScreen.class);
        startActivity(intent);
    }
}
