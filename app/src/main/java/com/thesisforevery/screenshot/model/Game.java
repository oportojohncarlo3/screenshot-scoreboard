package com.thesisforevery.screenshot.model;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class Game {
    @Id
    public long id;
    public int gameType;
    public String name;

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", gameType=" + gameType +
                ", name='" + name + '\'' +
                '}';
    }
}