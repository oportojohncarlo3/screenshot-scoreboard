package com.thesisforevery.screenshot.model;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class BasketballGame {
    @Id
    public long id;

    public String team1Name;
    public int team1Score;
    public String team1TeamFouls;

    public String team2Name;
    public int team2Score;
    public String team2TeamFouls;

    public String gameQtr;
    public String gameRemainingMinute;
    public String gameRemainingSeconds;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeam1Name() {
        return team1Name;
    }

    public void setTeam1Name(String team1Name) {
        this.team1Name = team1Name;
    }

    public int getTeam1Score() {
        return team1Score;
    }

    public void setTeam1Score(int team1Score) {
        this.team1Score = team1Score;
    }

    public String getTeam1TeamFouls() {
        return team1TeamFouls;
    }

    public void setTeam1TeamFouls(String team1TeamFouls) {
        this.team1TeamFouls = team1TeamFouls;
    }

    public String getTeam2Name() {
        return team2Name;
    }

    public void setTeam2Name(String team2Name) {
        this.team2Name = team2Name;
    }

    public int getTeam2Score() {
        return team2Score;
    }

    public void setTeam2Score(int team2Score) {
        this.team2Score = team2Score;
    }

    public String getTeam2TeamFouls() {
        return team2TeamFouls;
    }

    public void setTeam2TeamFouls(String team2TeamFouls) {
        this.team2TeamFouls = team2TeamFouls;
    }

    public String getGameQtr() {
        return gameQtr;
    }

    public void setGameQtr(String gameQtr) {
        this.gameQtr = gameQtr;
    }

    public String getGameRemainingMinute() {
        return gameRemainingMinute;
    }

    public void setGameRemainingMinute(String gameRemainingMinute) {
        this.gameRemainingMinute = gameRemainingMinute;
    }

    public String getGameRemainingSeconds() {
        return gameRemainingSeconds;
    }

    public void setGameRemainingSeconds(String gameRemainingSeconds) {
        this.gameRemainingSeconds = gameRemainingSeconds;
    }

    @Override
    public String toString() {
        return "\nBasketballGame{" +
                "id=" + id +
                ", team1Name='" + team1Name + '\'' +
                ", team1Score='" + team1Score + '\'' +
                ", team1TeamFouls='" + team1TeamFouls + '\'' +
                ", team2Name='" + team2Name + '\'' +
                ", team2Score='" + team2Score + '\'' +
                ", team2TeamFouls='" + team2TeamFouls + '\'' +
                ", gameQtr='" + gameQtr + '\'' +
                ", gameRemainingMinute='" + gameRemainingMinute + '\'' +
                ", gameRemainingSeconds='" + gameRemainingSeconds + '\'' +
                "}\n";
    }
}