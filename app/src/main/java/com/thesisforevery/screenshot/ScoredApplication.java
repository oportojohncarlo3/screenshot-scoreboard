package com.thesisforevery.screenshot;

import android.app.Application;
import android.util.Log;

import com.thesisforevery.screenshot.helper.ObjectBox;

public class ScoredApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("ScoredApplication", "onCreate");
        ObjectBox.init(this);
    }
}
